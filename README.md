Pig Latin
=========

An encoder of frivolous jargon.

![Screenshot](screenshot.png)

* Language: Vala
* Libraries: GTK, Granite
* OS: elementary OS Freya, Ubuntu 14.04

Building
------------

    # Install dev dependencies
    sudo apt install -y elementary-sdk libgtksourceview-3.0-dev

    # Clone the repo
    git clone https://github.com/alexgleason/pig-latin.git
    cd pig-latin

    # Build it
    mkdir -p build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr ../

    # Run it
    ./com.github.alexgleason.pig-latin
